"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var A = [1, 3, 4, 6, 7, 9];
var B = [1, 2, 4, 5, 9, 10]; // create an empty array called “common”
// concat A & B into single array called “elements”
// [1,1,2,3,4,4,5,6,7,9,9,10]
// create a dictionary called “seen” using Set
// for const “digit” of “elements”
// check if “seen” does not have “digit”
// if true “seen” add “digit”
// else
// push “digit” onto “common”
// return common

function commonElements_me(A, B) {
  var common = [];
  var elements = [].concat(_toConsumableArray(A), _toConsumableArray(B));
  var seen = new Set();
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = elements[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var digit = _step.value;

      if (!seen.has(digit)) {
        seen.add(digit);
      } else {
        if (common.indexOf(digit) === -1) {
          common.push(digit);
        }
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return common;
}

function commonElements(A, B) {
  var p1 = 0;
  var p2 = 0;
  var result = [];

  while (p1 < A.length && p2 < B.length) {
    if (A[p1] === B[p2]) {
      result.push(A[p1]);
      p1++;
      p2++;
    } else if (A[p1] > B[p2]) {
      p2++;
    }
  }

  return result;
}

var array1A = [1, 3, 4, 6, 7, 9];
var array2A = [1, 2, 4, 5, 9, 10]; // commonElements(array1A, array2A) should return [1, 4, 9] (an array).

console.log(commonElements(array1A, array2A));
var array1B = [1, 2, 9, 10, 11, 12];
var array2B = [0, 1, 2, 3, 4, 5, 8, 9, 10, 12, 14, 15]; // commonElements(array1B, array2B) should return [1, 2, 9, 10, 12] (an array).

console.log(commonElements(array1B, array2B));
var array1C = [0, 1, 2, 3, 4, 5];
var array2C = [6, 7, 8, 9, 10, 11]; // common_elements(array1C, array2C) should return [] (an empty array).

console.log(commonElements(array1C, array2C));